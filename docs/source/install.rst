
.. role:: blue

.. raw:: html

    <style> .blue {color:#1f618d} </style>
    <style> .red {color:red} </style>

Installing :blue:`CASCADe-jitter`
=================================

Using Anaconda
--------------

The easiest way is to create an anaconda environment
and to install and run :blue:`CASCADe-jitter` within this environment.
In case anaconda is not yet installed on the local system, start with
downloading the installer, which can be found at:

	https://www.anaconda.com/download/

Once installed update anaconda to the latest version:

.. code-block:: bash

  conda update conda

and then create and environment for :blue:`CASCADe-jitter`:

.. code-block:: bash

  conda create --name cascade-jitter python=3.9 ipython

Note that specifying the python version and adding ipython to be installed are
optional. The :blue: `CASCADe-jitter` code is guaranteed to work with python versions
3.7 to 3.9. The anaconda cascade-jitter environment can then be activated with
the following command:

.. code-block:: bash

  source activate cascade-jitter

One can now install all necessary packages for :blue:`CASCADe-jitter` within
this environment either with conda install or pip install. The :blue:`CASCADe-jitter`
package itself can be installed from GitLab or from the PyPi repositories.


Installing :blue:`CASCADe-jitter` with pip
---------------------------------------------

The easiest way to install the :blue:`CASCADe-jitter` package is to download
the distribution from PyPi, and install the package a designated Anaconda
environment with the following commands:

.. code-block:: bash

   conda activate cascade-jitter
   pip install CASCADe-jitter

This will install all code and scripts you need for the package to work.

Installing the :blue:`CASCADe-jitter` examples
-------------------------------------------------

The :blue:`CASCADe-jitter` package comes with
several examples, demonstrating how to detect and filter cosmic hits from
spectroscopic images.  If the package is installed from PypPi, the example
jupyter notebooks and simulated data need be downloaded from the GitLab
repository. To initialize the data download one can use the following bash command
in the Anaconda environment:

.. code-block:: bash

  setup_cascade-jitter.py

or alternatively from within the python interpreter:

.. code-block:: python

  from cascade_jitter.initialize import setup_examples
  setup_examples()


The additional downloaded data also includes examples and observational data to
try out the :blue:`CASCADe-jitter` package, which are explained
below.

.. note::
  NOTE: The data files will be downloaded by default to a **CASCADeSTORAGE/**
  directory in the users home directory. If a different location is preferred,
  please read the section on how to set the :blue:`CASCADe` environment variable
  **CASCADE_STORAGE_PATH** first. For details in the
  environment variables we refer to the documentation of the
  :blue:`CASCADe` main package.


Installing alternatives for the :blue:`CASCADe-jitter` package
-----------------------------------------------------------------

The :blue:`CASCADe-jitter` code can also be downloaded from
GitLab directly by either using git or pip. To download and install with a
single command using pip, type in a terminal the following command

.. code-block:: bash

  pip install git+git://gitlab.com/jbouwman/CASCADe-jitter.git@main

which will download the latest version. For other releases replace the ``main``
branch with one of the available releases on GitLab. Alternatively, one can first
clone the repository and then install, either using the HTTPS protocal:

.. code-block:: bash

  git clone https://gitlab.com/jbouwman/CASCADe-jitter.git

or clone using SSH:

.. code-block:: bash

  git clone git@gitlab.com:jbouwman/CASCADe-jitter.git

Both commands will download a copy of the files in a folder named after the
project's name. You can then navigate to the directory and start working on it
locally. After accessing the root folder from terminal, type

.. code-block:: bash

  pip install .

to install the package.

In case one is installing :blue:`CASCADe-jitter` directly from
GitLab, and one is using Anaconda,  make sure a cascade environment is created
and activated before using our package. For convenience, in the :blue:`CASCADe-jitter`
main package directory an **environment.yml** can be found. You can use this yml
file to create or update the cascade Anaconda environment. If you not already
had created an cascade environment execute the following command:

.. code-block:: bash

  conda env create -f environment.yml

In case you already have an cascade environment, you can update the necessary
packages with the following command (also use this after updating
:blue:`CASCADe-jitter` itself):

.. code-block:: bash

  conda env update -f environment.yml

Make sure the :blue:`CASCADe-jitter` package is in your path. You can either
set a ``PYTHONPATH`` environment variable pointing to the location of the
:blue:`CASCADe-jitter` package on your system, or when using anaconda with the
following command:

.. code-block:: bash

  conda develop <path_to_the_CASCADe_package_on_your_system>/CASCADe-jitter
