The cascade_filtering.least_squares module
==========================================

.. automodule:: cascade_filtering.least_squares
    :members:
    :undoc-members:
    :show-inheritance:
