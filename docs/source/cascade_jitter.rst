
.. role:: blue

.. raw:: html

    <style> .blue {color:#1f618d} </style>
    <style> .red {color:red} </style>

:blue:`CASCADe-jitter` API
==========================

.. toctree::

   cascade_jitter.jitter
   cascade_jitter.jitter_utilities
   cascade_jitter.initialize
   cascade_jitter.utilities
   cascade_jitter.least_squares
