## History of versions

**version 0.9.1**
- First online release

**version 0.9.2**
- First fully functional version

**version 0.9.3**
- Added a derotation before shift determination
- Updated documentation

**version 0.9.4**
- Fixed bug in rotation determination
- Added example notebook
- Fixed bug due to scikit-image release version.
